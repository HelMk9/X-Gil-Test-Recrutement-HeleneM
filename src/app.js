let isDrawing = false;
let startX, startY;
const rectangle = document.getElementById('rectangle');

/**
 * Start drawing*/
rectangle.addEventListener('mousedown', (e) => {
    isDrawing = true;
    startX = e.clientX;
    startY = e.clientY;
    rectangle.style.backgroundColor = "rgb("+Math.floor(Math.random() * 100)+","+Math.floor(Math.random() * 10)+","+Math.floor(Math.random() * 1000)+")"  
   });

/**
 * Stop drawing*/
rectangle.addEventListener('mouseup', () => {
isDrawing = false;
});  


document.addEventListener('mousemove', (e) => {
    if (isDrawing){
    const width = e.clientX - startX;
    const height = e.clientY - startY;
    rectangle.style.width = Math.abs(width) + 'px';
    rectangle.style.height = Math.abs(height) + 'px';
    rectangle.style.left = (width < 0) ? e.clientX + 'px' : startX + 'px';
    rectangle.style.top = (height < 0) ? e.clientY + 'px' : startY + 'px';
    }
});


/*delete rect at dblclick*/
rectangle.addEventListener('dblclick', rotateRectangle);

 /**
 * Function to rotate element at 360 degree
 */
function rotateRectangle() {
   
    let degre = 360
    let rotation = 0;
    const interval = setInterval(() => {
      rotation += 3;
      rectangle.style.transform = `rotate(${rotation}deg)`;
      if (rotation === degre) {
        clearInterval(interval);
      }
    }, 20);
    setTimeout(hidden,5000);
  }
 /**
     * create function for delete element
     *  
     */
function hidden(){
   
    document.getElementById("rectangle").style.visibility = "hidden";
}
